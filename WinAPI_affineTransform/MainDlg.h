#pragma once
#include "DlgBoxTemplate.h"
#include "PGAffineTransform.h"
#include <vector>
#include "resource.h"


class MainDlg : public DlgBoxTemplate
{
public:
	MainDlg(HINSTANCE hInstance, LPCSTR dlgResName, HWND hwndParent);
	~MainDlg();
	LRESULT RealDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void OnCreate();
private:	
	int cxClient, cyClient;
	MY_POINT affineCenter;
	int dx, dy;
	double angle;
	double minX, maxX, minY, maxY;
	std::vector<MY_POINT> characterVertices;
	std::vector<MY_POINT> numberTopArcS;
	std::vector<MY_POINT> numberTopArcB;
	std::vector<MY_POINT> numberBottomArcS;
	std::vector<MY_POINT> numberBottomArcB;
	std::vector<MY_POINT> numberTopVertices;
	std::vector<MY_POINT> numberMiddleVertices;
	std::vector<MY_POINT> numberBottomVertices;
	CHOOSECOLOR cc;
	COLORREF crCustColors[16];
	PGAffineTransform *transformManager;

	int toScreenX(double x);
	int toScreenY(double y);
	double toWorldX(int xs);
	double toWorldY(int yx);
	void drawLine(HDC &hdc, MY_POINT start, MY_POINT destination);
	void drawLines(HDC &hdc);
	MY_POINT CalculateBezierPoint(double t, MY_POINT p0, MY_POINT p1, MY_POINT p2, MY_POINT p3);
	void drawArc(HDC &hdc, MY_POINT p0, MY_POINT p1, MY_POINT p2, MY_POINT p3);
	void drawArcs(HDC &hdc);
	void setPoints();
};
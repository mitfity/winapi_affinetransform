#pragma once
#include <Windows.h>
#include <vector>

struct MY_POINT {
	double x;
	double y;
};

class PGAffineTransform
{
public:
	PGAffineTransform();
	~PGAffineTransform();

	void MoveUp(std::vector<MY_POINT> &points, int dy);
	void MoveDown(std::vector<MY_POINT> &points, int dy);
	void MoveRight(std::vector<MY_POINT> &points, int dx);
	void MoveLeft(std::vector<MY_POINT> &points, int dx);
	void PGAffineTransformScaleIncrease(std::vector<MY_POINT> &points);
	void PGAffineTransformScaleDecrease(std::vector<MY_POINT> &points);
	void PGAffineTransformRotate(std::vector<MY_POINT> &points, double angle, MY_POINT &affineCenter);
private: 
	void PointsVectorOnMatrix(std::vector<MY_POINT> &points, double T[3][3]);
	void PGAffineTransformScale(std::vector<MY_POINT> &points, double scale);
};


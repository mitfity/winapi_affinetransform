#include "PGAffineTransform.h"
#include <tchar.h>


PGAffineTransform::PGAffineTransform()
{
}


PGAffineTransform::~PGAffineTransform()
{
}

void PGAffineTransform::PointsVectorOnMatrix(std::vector<MY_POINT> &points, double T[3][3])
{
	double c[3] = { 0., 0., 1. };
	double d[3] = { 0., 0., 1. };

	for (int i = 0; i < points.size(); i++)
	{
		c[0] = points[i].x;
		c[1] = points[i].y;
		c[2] = 1.;

		for (int j = 0; j < 3; j++)
		{
			d[j] = 0.;
			for (int k = 0; k < 3; k++)
				d[j] += T[k][j] * c[k];
		}

		points[i].x = d[0];
		points[i].y = d[1];
	}
}

void PGAffineTransform::MoveUp(std::vector<MY_POINT> &points, int dy)
{
	double T[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ 0, -dy, 1 }
	};
	PointsVectorOnMatrix(points, T);
}

void PGAffineTransform::MoveDown(std::vector<MY_POINT> &points, int dy)
{
	double T[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ 0, dy, 1 }
	};
	PointsVectorOnMatrix(points, T);
}

void PGAffineTransform::MoveRight(std::vector<MY_POINT> &points, int dx)
{
	double T[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ dx, 0, 1 }
	};
	PointsVectorOnMatrix(points, T);
}

void PGAffineTransform::MoveLeft(std::vector<MY_POINT> &points, int dx)
{
	double T[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ -dx, 0, 1 }
	};
	PointsVectorOnMatrix(points, T);
}

void PGAffineTransform::PGAffineTransformScale(std::vector<MY_POINT> &points, double scale)
{
	double T[3][3] =
	{
		{ scale, 0, 0 },
		{ 0, scale, 0 },
		{ 0, 0, 1 }
	};
	PointsVectorOnMatrix(points, T);
}

void PGAffineTransform::PGAffineTransformScaleIncrease(std::vector<MY_POINT> &points)
{
	PGAffineTransformScale(points, 1.25);
}

void PGAffineTransform::PGAffineTransformScaleDecrease(std::vector<MY_POINT> &points)
{
	PGAffineTransformScale(points, 0.8);
}

void PGAffineTransform::PGAffineTransformRotate(std::vector<MY_POINT> &points, double angle, MY_POINT &affineCenter)
{
	double TM[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ -affineCenter.x, -affineCenter.y, 1 }
	};
	PointsVectorOnMatrix(points, TM);

	double TR[3][3] =
	{
		{ cos(angle), sin(angle), 0 },
		{ -sin(angle), cos(angle), 0 },
		{ 0, 0, 1 }
	};
	PointsVectorOnMatrix(points, TR);

	double TB[3][3] =
	{
		{ 1, 0, 0 },
		{ 0, 1, 0 },
		{ affineCenter.x, affineCenter.y, 1 }
	};
	PointsVectorOnMatrix(points, TB);
}
#include <Windows.h>
#include "MainDlg.h"

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	MainDlg *mainDlg = new MainDlg(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL);
	mainDlg->Show(SW_SHOW);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (msg.message == WM_KEYDOWN ||
			msg.message == WM_KEYUP)
			SendMessage(mainDlg->m_hWnd, msg.message,
			msg.wParam, msg.lParam);

		if (!IsDialogMessage(mainDlg->m_hWnd, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return 0;
}
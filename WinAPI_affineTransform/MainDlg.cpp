#include "MainDlg.h"
#include <tchar.h>

#define M_PI       3.14159265358979323846
#define M_PI_2     1.57079632679489661923
#define M_PI_4     0.785398163397448309616


MainDlg::MainDlg(HINSTANCE hInstance, LPCSTR dlgResName, HWND hwndParent)
	: DlgBoxTemplate(hInstance, dlgResName, hwndParent)
{
	OnCreate();
}


MainDlg::~MainDlg()
{
	delete transformManager;
}

void MainDlg::OnCreate()
{
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = m_hWnd;
	cc.hInstance = NULL;
	cc.rgbResult = RGB(0x80, 0x80, 0x80);
	cc.lpCustColors = crCustColors;
	cc.Flags = CC_RGBINIT | CC_FULLOPEN;
	cc.lCustData = 0L;
	cc.lpfnHook = NULL;
	cc.lpTemplateName = NULL;

	affineCenter.x = affineCenter.y = 200;
	//Move speed
	dx = dy = 6;
	angle = M_PI_4;

	transformManager = new PGAffineTransform();
}

/*
------------------------------------------
			Coordinates
------------------------------------------
*/
int MainDlg::toScreenX(double x)
{
	return (int)(cxClient * (x - minX) / (maxX - minX));
}

int MainDlg::toScreenY(double y)
{
	return (int)(cyClient * (1 - (y - minY) / (maxY - minY)));
}

double MainDlg::toWorldX(int xs) {
	return 1.0 * xs / cxClient * (maxX - minX) + minX;
}

double MainDlg::toWorldY(int ys) {
	return (1.0 * ys - cyClient) /
		(-cyClient) * (maxY - minY) + minY;
}


/*
------------------------------------------
			Set start points
------------------------------------------
*/
void MainDlg::setPoints()
{
	characterVertices = {
			{ 100., 100. }, { 225., 100. }, { 225., 150. }, { 150., 150. },
			{ 150., 300. }, { 100., 300. }, { 100., 100. }
	};

	numberTopArcB = {
			{ 400., 100. }, { 480., 100. }, { 480., 200. }, { 400., 200. }
	};

	numberTopArcS = {
			{ 400., 125. }, { 440., 125. }, { 440., 175. }, { 400., 175. }
	};

	numberBottomArcB = {
			{ 400., 200. }, { 480., 200. }, { 480., 300. }, { 400., 300. }
	};

	numberBottomArcS = {
			{ 400., 225. }, { 440., 225. }, { 440., 275. }, { 400., 275. }
	};

	numberTopVertices = {
			{ 400., 100. }, { 362, 100. }, { 362, 125. }, { 400., 125. }
	};

	numberMiddleVertices = {
			{ 400., 175. }, { 374., 175. }, { 374., 225. }, { 400., 225. }
	};

	numberBottomVertices = {
			{ 400., 275. }, { 350., 275. }, { 350., 300. }, { 400., 300. }
	};
}

/*
------------------------------------------
			Brezenkhem
------------------------------------------
*/
void MainDlg::drawLine(HDC &hdc, MY_POINT start, MY_POINT destination)
{
	int x1 = round(start.x), x2 = round(destination.x), y1 = round(start.y), y2 = round(destination.y);

	bool steep = false;
	if (std::abs(x1 - x2) < std::abs(y1 - y2)) {
		std::swap(x1, y1);
		std::swap(x2, y2);
		steep = true;
	}
	if (x1 > x2) {
		std::swap(x1, x2);
		std::swap(y1, y2);
	}
	int dx = x2 - x1;
	int dy = y2 - y1;
	int error = std::abs(dy) * 2;
	int c = 0;
	int y = y1;
	for (int x = x1; x <= x2; x++) {
		if (steep) {
			SetPixel(hdc, y, x, cc.rgbResult);
		}
		else {
			SetPixel(hdc, x, y, cc.rgbResult);
		}
		c += error;

		if (c > dx) {
			y += (y2 > y1 ? 1 : -1);
			c -= dx * 2;
		}
	}
}

void MainDlg::drawLines(HDC &hdc)
{
	/*------------> Draw character lines <------------*/
	MY_POINT previousPoint = characterVertices[0];
	for (int i = 1; i < characterVertices.size(); i++)
	{
		
		drawLine(hdc, previousPoint, characterVertices[i]);
		previousPoint = characterVertices[i];
	}

	/*------------> Draw number lines <------------*/
	previousPoint = numberTopVertices[0];
	for (int i = 1; i < numberTopVertices.size(); i++)
	{
		/*TCHAR mystring[100];
		_stprintf(mystring, _T("x : %f y : %f\n"), numberTopVertices[i].x, numberTopVertices[i].y);
		OutputDebugString(mystring);*/
		drawLine(hdc, previousPoint, numberTopVertices[i]);
		previousPoint = numberTopVertices[i];
	}
	previousPoint = numberMiddleVertices[0];
	for (int i = 1; i < numberMiddleVertices.size(); i++)
	{
		drawLine(hdc, previousPoint, numberMiddleVertices[i]);
		previousPoint = numberMiddleVertices[i];
	}
	previousPoint = numberBottomVertices[0];
	for (int i = 1; i < numberBottomVertices.size(); i++)
	{
		drawLine(hdc, previousPoint, numberBottomVertices[i]);
		previousPoint = numberBottomVertices[i];
	}
}

MY_POINT MainDlg::CalculateBezierPoint(double t, MY_POINT p0, MY_POINT p1, MY_POINT p2, MY_POINT p3)
{
	double u = 1 - t;
	double tt = t*t;
	double uu = u*u;
	double uuu = uu * u;
	double ttt = tt * t;

	MY_POINT p = { p0.x*uuu, p0.y*uuu };					//first term
	p.x += p1.x * 3 * uu * t; p.y += p1.y * 3 * uu * t;		//second term
	p.x += p2.x * 3 * u * tt; p.y += p2.y * 3 * u * tt;		//third term
	p.x += p3.x * ttt; p.y += p3.y * ttt;					//fourth term    

	return p;
}

void MainDlg::drawArc(HDC &hdc, MY_POINT p0, MY_POINT p1, MY_POINT p2, MY_POINT p3)
{
	double t;
	MY_POINT q0 = CalculateBezierPoint(0, p0, p1, p2, p3);
	MY_POINT q1;
	int ptsCount = sqrt( (p3.y - p0.y)*(p3.y - p0.y) + (p3.x - p3.x)*(p3.x - p3.x) ) * 1.5;
	if ((p0.y == p3.y) || ( abs(p0.y-p3.y) <= 1 ))
		ptsCount = (p3.x > p0.x) ? p3.x - p0.x : p0.x - p3.x;
	if ( (p0.x == p3.y) || ( abs(p0.x-p3.x) <= 1 ))
		ptsCount = (p3.y > p0.y) ? p3.y - p0.y : p0.y - p3.y;

	for (int i = 1; i <= ptsCount; i++)
	{
		t = i / (double)ptsCount;
		q1 = CalculateBezierPoint(t, p0, p1, p2, p3);
		drawLine(hdc, q0, q1);
		q0 = q1;
	}
}

void MainDlg::drawArcs(HDC &hdc)
{
	//Draw top arcs
	drawArc(hdc, numberTopArcB[0], numberTopArcB[1], numberTopArcB[2], numberTopArcB[3]);
	drawArc(hdc, numberTopArcS[0], numberTopArcS[1], numberTopArcS[2], numberTopArcS[3]);

	//Draw bottom arcs
	drawArc(hdc, numberBottomArcB[0], numberBottomArcB[1], numberBottomArcB[2], numberBottomArcB[3]);
	drawArc(hdc, numberBottomArcS[0], numberBottomArcS[1], numberBottomArcS[2], numberBottomArcS[3]);
}

/*
------------------------------------------
			WinAPI
------------------------------------------
*/
LRESULT MainDlg::RealDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	HDC hdcBuf;
	RECT rect;
	HBITMAP hbmBuf;
	HBITMAP hOldBmp;
	
	switch (uMsg)
	{
	case WM_SIZE:
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);
		setPoints();
		return 0;
	case WM_COMMAND:
		if (wParam == IDCOLOUR)
		{
			if (ChooseColor(&cc))
				RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
		}
		if (wParam == IDC_BUTTON_INC)
		{
			transformManager->PGAffineTransformScaleIncrease(characterVertices);
			transformManager->PGAffineTransformScaleIncrease(numberTopArcB);
			transformManager->PGAffineTransformScaleIncrease(numberTopArcS);
			transformManager->PGAffineTransformScaleIncrease(numberTopVertices);
			transformManager->PGAffineTransformScaleIncrease(numberMiddleVertices);
			transformManager->PGAffineTransformScaleIncrease(numberBottomArcB);
			transformManager->PGAffineTransformScaleIncrease(numberBottomArcS);
			transformManager->PGAffineTransformScaleIncrease(numberBottomVertices);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
		}
		if (wParam == IDC_BUTTON_DEC)
		{
			transformManager->PGAffineTransformScaleDecrease(characterVertices);
			transformManager->PGAffineTransformScaleDecrease(numberTopArcB);
			transformManager->PGAffineTransformScaleDecrease(numberTopArcS);
			transformManager->PGAffineTransformScaleDecrease(numberTopVertices);
			transformManager->PGAffineTransformScaleDecrease(numberMiddleVertices);
			transformManager->PGAffineTransformScaleDecrease(numberBottomArcB);
			transformManager->PGAffineTransformScaleDecrease(numberBottomArcS);
			transformManager->PGAffineTransformScaleDecrease(numberBottomVertices);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
		}
		if (wParam == IDC_BUTTON_ROT)
		{
			transformManager->PGAffineTransformRotate(characterVertices, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberTopArcB, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberTopArcS, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberTopVertices, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberMiddleVertices, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberBottomArcB, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberBottomArcS, angle, affineCenter);
			transformManager->PGAffineTransformRotate(numberBottomVertices, angle, affineCenter);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
		}
		break;
	case WM_LBUTTONDOWN:		
		affineCenter.x = LOWORD(lParam);
		affineCenter.y = HIWORD(lParam);		
		RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
		break;
	case WM_ERASEBKGND:
		return TRUE;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_UP:
			transformManager->MoveUp(characterVertices, dx);
			transformManager->MoveUp(numberTopArcB, dx);
			transformManager->MoveUp(numberTopArcS, dx);
			transformManager->MoveUp(numberTopVertices, dx);
			transformManager->MoveUp(numberMiddleVertices, dx);
			transformManager->MoveUp(numberBottomArcB, dx);
			transformManager->MoveUp(numberBottomArcS, dx);
			transformManager->MoveUp(numberBottomVertices, dx);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
			break;
		case VK_DOWN:
			transformManager->MoveDown(characterVertices, dx);
			transformManager->MoveDown(numberTopArcB, dx);
			transformManager->MoveDown(numberTopArcS, dx);
			transformManager->MoveDown(numberTopVertices, dx);
			transformManager->MoveDown(numberMiddleVertices, dx);
			transformManager->MoveDown(numberBottomArcB, dx);
			transformManager->MoveDown(numberBottomArcS, dx);
			transformManager->MoveDown(numberBottomVertices, dx);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
			break;
		case VK_LEFT:
			transformManager->MoveLeft(characterVertices, dy);
			transformManager->MoveLeft(numberTopArcB, dy);
			transformManager->MoveLeft(numberTopArcS, dy);
			transformManager->MoveLeft(numberTopVertices, dy);
			transformManager->MoveLeft(numberMiddleVertices, dy);
			transformManager->MoveLeft(numberBottomArcB, dy);
			transformManager->MoveLeft(numberBottomArcS, dy);
			transformManager->MoveLeft(numberBottomVertices, dy);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
			break;
		case VK_RIGHT:
			transformManager->MoveRight(characterVertices, dy);
			transformManager->MoveRight(numberTopArcB, dy);
			transformManager->MoveRight(numberTopArcS, dy);
			transformManager->MoveRight(numberTopVertices, dy);
			transformManager->MoveRight(numberMiddleVertices, dy);
			transformManager->MoveRight(numberBottomArcB, dy);
			transformManager->MoveRight(numberBottomArcS, dy);
			transformManager->MoveRight(numberBottomVertices, dy);
			RedrawWindow(m_hWnd, 0, 0, RDW_INVALIDATE);
			break;
		default:
			break;
		}
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		hdcBuf = CreateCompatibleDC(hdc);

		hbmBuf = CreateCompatibleBitmap(hdc, rect.right,rect.bottom);
		hOldBmp = (HBITMAP)SelectObject(hdcBuf, hbmBuf);
		Rectangle(hdcBuf, rect.left, rect.top, rect.right, rect.bottom);
		
		/*------------> Draw points <------------*/
		drawLines(hdcBuf);
		drawArcs(hdcBuf);

		Ellipse(hdcBuf, affineCenter.x - 3, affineCenter.y - 3, affineCenter.x + 3, affineCenter.y + 3);

		BitBlt(hdc, 0, 0, rect.right, rect.bottom, hdcBuf, 0, 0, SRCCOPY);
		SelectObject(hdcBuf, hOldBmp);
		DeleteDC(hdcBuf);
		DeleteObject(hbmBuf);
		EndPaint(hwnd, &ps);
		break;
	}

	return DlgBoxTemplate::RealDlgProc(hwnd, uMsg, wParam, lParam);
}